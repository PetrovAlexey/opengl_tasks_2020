#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform vec3 positions[1000];

uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoord;

out vec3 normalCamSpace;
out vec4 posCamSpace;
out vec2 texCoord;

void main()
{
	texCoord = vertexTexCoord;

	vec3 modelPosition = positions[gl_InstanceID];

	posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition + modelPosition, 1.0);
	normalCamSpace = normalize(normalToCameraMatrix * vertexNormal);

	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition + modelPosition, 1.0);
}
