#version 330
uniform sampler2D diffuseTex;

struct LightInfo
{
	vec3 dir;
	vec3 La;
	vec3 Ld;
	vec3 Ls;
};
uniform LightInfo light;

out vec4 fragColor;

in vec3 normalCamSpace;
in vec4 posCamSpace;
in vec2 texCoord;


const vec3 Ks = vec3(0.5, 0.5, 0.5);
const float shininess = 128.0;

void main()
{
	vec4 tex = texture(diffuseTex, texCoord);

	float alpha = tex.a;
    vec3 diffuseColor = tex.rgb;
    if( alpha == 0 ) {
        discard;
    }


	vec3 good_normal = tex.xyz * 2.0 - 1.0;

	vec3 normal = normalize(normalCamSpace + 0.1*good_normal);
	
	vec3 viewDirection = normalize(-posCamSpace.xyz);
	vec3 lightDirCamSpace = normalize(light.dir);

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0);
    vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

	if (NdotL > 0.0)
	{			
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);
		float blinnTerm = max(dot(normal, halfVector), 0.0);
		blinnTerm = pow(blinnTerm, shininess);
		color += light.Ls * Ks * blinnTerm;
	}
	
	fragColor = vec4(color, alpha);
}
