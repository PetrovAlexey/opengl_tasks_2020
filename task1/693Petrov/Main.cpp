#include <glm/glm.hpp>
#include <cassert>

#include <iostream>
#include <vector>
#include <random>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/LightInfo.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"

#include "LSystem.hpp"
#include "Tree.hpp"

namespace {
float frand(float a, float b) {
    static std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution(a, b);
    return distribution(generator);
}
}


class ForestApplication : public Application
{
public:

    std::pair<MeshPtr, MeshPtr> _tree;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _shaderInstanced;
    ShaderProgramPtr _cullShader;

    TexturePtr bufferTexCulled;
    GLuint _TF; //������ ��� �������� ��������� Transform Feedback
    GLuint _cullVao; //VAO, ������� ������ ��������� ������ ��� ������ ������ �� ����� Transform Feedback
    GLuint _tfOutputVbo; //VBO, � ������� ����� ������������ �������� ������� ����� ���������
    DataBufferPtr treeModelPosBuf;
    GLuint _query; //����������-�������, ���� ����� ������������ ���������� ���������� ����� �������

    TexturePtr _bufferTexAll;
    TexturePtr _bufferTexCulled;
   
    GLuint _sampler;

    TexturePtr leafTexture;
    TexturePtr barkTexture;
    TexturePtr _grassTexture;

    float phi = glm::pi<float>() * 0.0f;
    float theta = glm::pi<float>() * 0.25f;

    //���������� ��� ���������� ���������� ������ ��������� �����
    float _lr = 10.0;
    float _phi = glm::pi<float>() * 0.5f;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light;

    GLuint _leafSampler;

    MeshPtr _ground;
    GLuint _sceneSampler;

    std::vector<glm::vec3> _positionsVec3;

    void makeScene() override {
        Application::makeScene();

        leafTexture = loadTexture("leaves.png");
        barkTexture = loadTexture("bark.png");
        _grassTexture = loadTexture("grass.png");


        LSystem lsystem;
        lsystem.setInitialString("F");
        //lsystem.addRule('F', "F[-F]&[F]^^[F]&[+F][^F]");
        lsystem.addRule('F', "F[-F]&[F]^^[F]&[+F][F]");

        lsystem.buildSystem(4);
        _tree = makeTreeL(lsystem.draw());
        _tree.first->setModelMatrix(glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(0.5f, 0.5f, 0.5f)), glm::vec3(0.0f, 0.0f, 0.0f)));
        _tree.second->setModelMatrix(glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(0.5f, 0.5f, 0.5f)), glm::vec3(0.0f, 0.0f, 0.0f)));

        const float size = 10.0f;
        for (unsigned int i = 0; i < 10; i++)
        {
            _positionsVec3.push_back(glm::vec3(frand(-size, size), 0, frand(-size, size)));
        }

        _ground = makeGroundPlane(30.0f, 5.0f);
        _ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

        _shader = std::make_shared<ShaderProgram>("693PetrovData/shader.vert", "693PetrovData/shader.frag");
        _shaderInstanced = std::make_shared<ShaderProgram>("693PetrovData/instancingTexture.vert",
            "693PetrovData/shader.frag");

        treeModelPosBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        treeModelPosBuf->setData(10 * sizeof(float) * 3, _positionsVec3.data());

        // �������������� �����, � ������� ����� ����������� �������� ������� ����� ���������
        glGenBuffers(1, &_tfOutputVbo);
        glBindBuffer(GL_ARRAY_BUFFER, _tfOutputVbo);
        glBufferData(GL_ARRAY_BUFFER, 10 * sizeof(float) * 3, 0, GL_STREAM_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        //������� ���������� ����� � ����������� � ���� ����� ��� ������������
        _bufferTexAll = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        _bufferTexAll->bind();
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, treeModelPosBuf->id());
        _bufferTexAll->unbind();

        //������� ���������� ����� � ����������� � ���� ����� ��� ������������
        _bufferTexCulled = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        _bufferTexCulled->bind();
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _tfOutputVbo);
        _bufferTexCulled->unbind();


        _cullShader = std::make_shared<ShaderProgram>();
        ShaderPtr vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
        vs->createFromFile("693PetrovData/cull.vert");
        _cullShader->attachShader(vs);
        ShaderPtr gs = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
        gs->createFromFile("693PetrovData/cull.geom");
        _cullShader->attachShader(gs);

        //�������� ����������, ������� ����� �������� � �����
        const char* attribs[] = { "position" };
        glTransformFeedbackVaryings(_cullShader->id(), 1, attribs, GL_SEPARATE_ATTRIBS);

        _cullShader->linkProgram();
        //----------------------------

        //VAO, ������� ����� ���������� ������ ��� ���������
        glGenVertexArrays(1, &_cullVao);
        glBindVertexArray(_cullVao);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, treeModelPosBuf->id());
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindVertexArray(0);

        //----------------------------

        //����������� ������ Transform Feedback
        glGenTransformFeedbacks(1, &_TF);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, _tfOutputVbo);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

        //----------------------------

        glGenQueries(1, &_query);

        _light.position = glm::vec3(glm::cos(phi) * glm::cos(theta), glm::sin(theta), glm::sin(phi) * glm::cos(theta));
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(0.3, 0.3, 0.3);

        glGenSamplers(1, &_leafSampler);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_sceneSampler);
        glSamplerParameteri(_sceneSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sceneSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sceneSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sceneSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    void cull(const ShaderProgramPtr& shader) {
        shader->use();

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader->setFloatUniform("treeH", 10);
        shader->setFloatUniform("treeR", 1.0);

        glEnable(GL_RASTERIZER_DISCARD);

        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);

        glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, _query);

        glBeginTransformFeedback(GL_POINTS);

        glBindVertexArray(_cullVao);
        glDrawArrays(GL_POINTS, 0, 10);

        glEndTransformFeedback();

        glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

        glDisable(GL_RASTERIZER_DISCARD);
    }

    void drawGrass() {
        //������������� ������.
        _shader->use();

        //������������� ����� �������-����������
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));

        _shader->setVec3Uniform("light.dir", lightDirCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        /*_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta),
            glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //�������� ��������� ��� � ������� ����������� ������
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);*/

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _grassTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        _shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix",
            glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
        _ground->draw();

        //����������� ������� � ��������� ���������
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawTrees() {
        GLuint primitivesWritten;
        glGetQueryObjectuiv(_query, GL_QUERY_RESULT, &primitivesWritten);

        //������������� ������.
        _shaderInstanced->use();

        //������������� ����� �������-����������
        _shaderInstanced->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shaderInstanced->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));

        _shaderInstanced->setVec3Uniform("light.dir", lightDirCamSpace);
        _shaderInstanced->setVec3Uniform("light.La", _light.ambient);
        _shaderInstanced->setVec3Uniform("light.Ld", _light.diffuse);
        _shaderInstanced->setVec3Uniform("light.Ls", _light.specular);

        /*
        _light.position =
            glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shaderInstanced->setVec3Uniform("light.pos",
            lightPosCamSpace); //�������� ��������� ��� � ������� ����������� ������
        _shaderInstanced->setVec3Uniform("light.La", _light.ambient);
        _shaderInstanced->setVec3Uniform("light.Ld", _light.diffuse);
        _shaderInstanced->setVec3Uniform("light.Ls", _light.specular);*/

        glActiveTexture(GL_TEXTURE1);
        _bufferTexCulled->bind();
        _shaderInstanced->setIntUniform("texBuf", 1);

        glEnable(GL_BLEND);
        glBlendColor(0.7, 0.7, 0.7, 0.7);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendFunc(GL_CONSTANT_COLOR, GL_ONE_MINUS_CONSTANT_COLOR);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        leafTexture->bind();
        _shaderInstanced->setIntUniform("diffuseTex", 0);

        _shaderInstanced->setMat3Uniform("normalToCameraMatrix",
            glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
        _tree.second->drawInstanced(primitivesWritten);

        glDisable(GL_BLEND);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        barkTexture->bind();
        _shaderInstanced->setIntUniform("diffuseTex", 0);

        _shaderInstanced->setMat3Uniform("normalToCameraMatrix",
            glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
        _tree.first->drawInstanced(primitivesWritten);

        //����������� ������� � ��������� ���������
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void draw() override
    {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        drawGrass();
        cull(_cullShader);
       
        drawTrees();
        /*_shader->use();



        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));

        _shader->setVec3Uniform("light.dir", lightDirCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sceneSampler);
        _grassTexture->bind();
        _ground->draw();

        //cull(_cullShader);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _leafSampler);
        leafTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);
        _shader->setVec3Uniforms("positions", _positionsVec3);
        _shader->setMat4Uniform("modelMatrix", _tree.second->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree.second->modelMatrix()))));
        _tree.second->drawInstanced(_positionsVec3.size());

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        barkTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);
        _shader->setVec3Uniforms("positions", _positionsVec3);
        _shader->setMat4Uniform("modelMatrix", _tree.first->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree.first->modelMatrix()))));
        _tree.first->drawInstanced(_positionsVec3.size());

        

        glBindSampler(0, 0);
        glUseProgram(0);*/
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

        }
        ImGui::End();
    }

};


int main()
{
    ForestApplication app;
    app.start();

    return 0;
}
