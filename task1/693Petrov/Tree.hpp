#pragma once

#include <vector>
#include "common/Mesh.hpp"
#include "LSystem.hpp"

std::pair<MeshPtr, MeshPtr> makeTreeL(const std::vector<LSystemLine>& lines);

